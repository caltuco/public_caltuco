variable "aws_access_key" {
  description = "Aws Access Key Gioseg"
}

variable "aws_secret_key" {
  description = "Aws Secret key"
}

variable "aws_region" {
  default     = "us-east-2"
  description = "AWS Region"
}

variable "aws_ssh_admin_key_file" {
  description = "ssh_pem_file"
}
