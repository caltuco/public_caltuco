import csv
import pandas as pd

def save_values(name, file, check_time, level):
	with open(file, mode='a') as water_level_file:
		water_level_writer = csv.writer(water_level_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

		water_level_writer.writerow([name, check_time, level])




#save_values('water_level.csv', 'Hora de revision', 'Nivel')

def read_values(file):
	water_level_pd = pd.read_csv(file)
	last_water_level_pd = water_level_pd[-1:] #Getting last row of the CSV file
	return last_water_level_pd


real_water_level = read_values('water_level.csv')
print(real_water_level)