import telebot
import file_utils as fs_utls 
from time import gmtime, strftime


token = ''

bot = telebot.TeleBot(token)

file = 'water_level.csv'
name = 'Luis Aguilar'

knownUsers = []  # todo: save these in a file,
userStep = {}  # so they won't reset every time the bot restarts

commands = {  # command description used in the "help" command
    'start'       : 'Get used to the bot',
    'help'        : 'Gives you information about the available commands',
    'agua'        : 'Envia el estado del tanque del agua',
    'getImage'    : 'A test using multi-stage messages, custom keyboard, and media sending'
}


def get_user_step(uid):
    if uid in userStep:
        return userStep[uid]
    else:
        knownUsers.append(uid)
        userStep[uid] = 0
        print("New user detected, who hasn't used \"/start\" yet")
        return 0


@bot.message_handler(commands=['start'])
def command_start(m):
    cid = m.chat.id
    if cid not in knownUsers:  # if user hasn't used the "/start" command yet:
        knownUsers.append(cid)  # save user id, so you could brodcast messages to all users of this bot later
        userStep[cid] = 0  # save user id and his current "command level", so he can use the "/getImage" command
        bot.send_message(cid, "Hola no te conozco aun...")
        bot.send_message(cid, "Sin embargo ya guarde tu numero")
        command_help(m)  # show the new user the help page
    else:
        bot.send_message(cid, "Ya se quien eres tu, tranquilo!!")

######################################################################

@bot.message_handler(commands=['agua'])
def start_message(message):
    markup = telebot.types.InlineKeyboardMarkup()
    markup.add(telebot.types.InlineKeyboardButton(text='4/4', callback_data=4))
    markup.add(telebot.types.InlineKeyboardButton(text='3/4', callback_data=3))
    markup.add(telebot.types.InlineKeyboardButton(text='2/4', callback_data=2))
    markup.add(telebot.types.InlineKeyboardButton(text='1/4', callback_data=1))
    bot.send_message(message.chat.id, text="Que nivel de agua tiene el tanque en estos momentos?", reply_markup=markup)


@bot.callback_query_handler(func=lambda call: True)
def query_handler(call):

    bot.answer_callback_query(callback_query_id=call.id, text='Respuesta recibida!')
    answer = 'Nivel de agua actualizado'
    actual_time = strftime("%d %b %Y %H:%M:%S", gmtime())
    if call.data == '1':
    	fs_utls.save_values(name, file, actual_time, call.data)
    # if call.data == '4':
    #     answer = 'You answered correctly!'

    bot.send_message(call.message.chat.id, answer)
    bot.edit_message_reply_markup(call.message.chat.id, call.message.message_id)

######################################################################


@bot.message_handler(commands=['help'])
def command_help(m):
    cid = m.chat.id
    help_text = "The following commands are available: \n"
    for key in commands:  # generate help text out of the commands dictionary defined at the top
        help_text += "/" + key + ": "
        help_text += commands[key] + "\n"
    bot.send_message(cid, help_text)  # send the generated help page

bot.polling()
