import pickle as pickle
import json 

def read_values(file):
		values_d = dict()
		values_d = json.load( open(file))
		return values_d


def write_values(file, browser_info):
	with open(file, "wb") as f:
		json.dump(browser_info, open(file, 'w'))
		
	return True

