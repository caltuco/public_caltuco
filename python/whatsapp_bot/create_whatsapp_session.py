from selenium import webdriver 
import time
from file_utils import *
from crop_image import crop_image
from pyvirtualdisplay import Display
import telebot


file = '.browser_data' #path to file to read pid 
path_name_screeshot = "screenshot.png" #creen shot's path 
browser_info = {}

telegram_file = 'telegram.token' #path to file to read token 
chatid = '126402979'

def read_values(file):
        values_l = list()
        with open (telegram_file, "r") as f:
            for line in f:
                values_l.append(line.strip()) 

        return values_l[1]


TELEGRAM_TOKEN = read_values(telegram_file)

bot = telebot.TeleBot(TELEGRAM_TOKEN)







#create a virtual display to headless browser
#display = Display(visible=0, size=(1024, 768)) 
#display.start()

driver = webdriver.Chrome()
driver.get('https://web.whatsapp.com/')
url = driver.command_executor._url
session_id = driver.session_id
browser_info["url"] = url
browser_info["session_id"] = session_id 

#qr = driver.find_element_by_xpath('//*[@id="app"]/div/div/div[2]/div[1]/div/div[2]/div/canvas')
write_values(file, browser_info)


#saving screen shot 
driver.save_screenshot(path_name_screeshot)
#cropping QR from screenshot 
crop_image(path_name_screeshot, 627, 146, 904, 421, 'qr_whatsapp.png')

bot.send_photo(chatid, photo=open('qr_whatsapp.png','rb'))


# """
# Firts option where we'll try to get QR of whatsapp using screenshot option, 
# sending QR cropped to Telegram bot in order to able to scan from the telephone

# """
