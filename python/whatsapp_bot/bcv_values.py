#!/usr/bin/python3
import sys
import datetime
import requests
from bs4 import BeautifulSoup


def bcv_usd_consult ():
	URL = 'http://www.bcv.org.ve/estadisticas/tipo-de-cambio'
	pagina = requests.get(URL)
	soup = BeautifulSoup(pagina.content, 'html.parser')
	results = soup.find(id='dolar')
	#print(results.prettify())
	job_elems = results.find_all('div', class_='col-sm-6 col-xs-6')
    

	for job_elem in job_elems:
		job_elem = (job_elem.text.strip())
	
	result_d = {}
	#result = '"Dolar BCV" : {} , "Hora" : {:%Y-%m-%d %H:%M:%S}'.format(job_elem, datetime.datetime.now())
	result_d["Dolar BCV"] = job_elem
	result_d["Consultado"] = '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())
	return result_d 