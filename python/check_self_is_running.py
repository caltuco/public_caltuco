#/usr/bin/env python3
# this script check before start if another copy itself is running, you must call from main script 
import os
import sys
# it must receive this values from main script
#pid = str(os.getpid())
#name_file_script = os.path.basename(__file__)

def check_is_running (name_file_script,pid):
	pidfile = "/tmp/.{}.pid".format(name_file_script)
	if os.path.isfile(pidfile):
	    print ("this script is already running, exiting".format(pidfile))
	    sys.exit()
	open(pidfile, 'w').write(pid)

def delete_script_mark (name_file_script):
	try:

		pidfile = "/tmp/.{}.pid".format(name_file_script)
		os.unlink(pidfile)
	except FileNotFoundError:
		print("No such file {}".format(pidfile))



