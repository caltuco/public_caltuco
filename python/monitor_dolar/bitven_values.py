#!/usr/bin/python3
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time
import datetime
from crop_image import crop_image
from ocr_to_image import get_values_from_images


def bitven_dolar_consult():
	result_d = {}

	path_name_screeshot = "screenshot.png"

	display = Display(visible=0, size=(1024, 768)) 
	display.start() 
	chrome_options = webdriver.ChromeOptions()
	chrome_options.add_argument("--headless")
	driver = webdriver.Chrome()
	driver.implicitly_wait(30)
	timeout = 3
	driver.get('https://www.bitven.com/')
	### se hizo una trampa para poder esperar que cargara el valor del precio, esperando  un titulo que aparece justo antes
	### del valor del precio 
	time.sleep(10)

	#taking a screenshot from webpage
	driver.save_screenshot(path_name_screeshot)

	#cropping from screenshot to take values using ocr
	crop_image(path_name_screeshot, 267, 325, 575, 360, 'bs_btc.png') 
	bs_btc = float(get_values_from_images('bs_btc.png'))

	crop_image(path_name_screeshot, 267, 392, 575, 430, 'usd_btc.png') 
	usd_btc = float(get_values_from_images('usd_btc.png'))


	WebDriverWait(driver, 50).until(EC.presence_of_element_located((By.XPATH, '//*[@id="dbtcHeadlines"]/h3')))
	price_bsf_usd_from_web = driver.find_elements_by_xpath('//*[@id="lechugas"]')
	unofficial_dolar = price_bsf_usd_from_web[0].text

	result_d["Paralelo"] = unofficial_dolar
	result_d["Bs x BTC"] = bs_btc
	result_d["USD x BTC"] = usd_btc

	#print (price_bsf_usd_from_web[0].text)
	#print (type(bs_btc))
	#print (usd_btc)
	


	# print (price_bsf_usd_from_web[0]) # valor crudo 
	# print (price_bsf_btc_from_web[0].text) # valor convertido a text para ser analizado

	# for value in price_bsf_usd_from_web:
	# 	price_bsf_usd =(value.text)

	# print(price_bsf_usd)
	driver.quit()

	return result_d

