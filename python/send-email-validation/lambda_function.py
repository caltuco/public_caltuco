import boto3
import os
from send_mail_validation import * 


def lambda_handler(event, context):
	
	# probar con STS para las credenciales

    sts_client = boto3.client('sts')

    role_assumed = sts_client.assume_role(
		RoleArn='arn:aws:iam::781563555508:role/ses_lambda',
		RoleSessionName='seslambda')

    credentials=role_assumed['Credentials']

    ses=boto3.client(
    	'ses', 
    	aws_access_key_id=credentials['AccessKeyId'], 
    	aws_secret_access_key=credentials['SecretAccessKey'], 
    	aws_session_token=credentials['SessionToken'],)




	#session = boto3.session.Session(profile_name='infra-e-digital')

	#ses = session.client('ses')

	#declaring list

    mails = []
	#selecting template regard to language, must be create first the template
    language = 'edigitalTemplatespanish'

	#just using to add to email to be validated 
	#mails.append('dfsdf@e-digital.cl')

    send_email_validation(ses)
	#add_email_validation(ses, mails, language)



