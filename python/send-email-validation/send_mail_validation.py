#### Used to send a email validation to user where are in status pending verification on SES Services of the system




def send_email_validation(ses):

	full_list_mails = ses.list_identities(
	    IdentityType='EmailAddress',
	    NextToken='',
	    MaxItems=123,
	)

	#print(full_list_mails['Identities'])

	validated_list_mails = ses.list_verified_email_addresses()


	#print(validated_list_mails['VerifiedEmailAddresses'])


	#getting delta for mails, where found as distinct of verified status 
	delta_list_mails = set(full_list_mails['Identities']) - set(validated_list_mails['VerifiedEmailAddresses'])


	#print(delta_list_mails)

	for mail in delta_list_mails:
		#print(mail)
		sent_mail_to = ses.send_custom_verification_email(
		    EmailAddress=mail,
		    TemplateName='edigitalTemplatespanish',
		     )


def add_email_validation(ses, mails, language):
	for mail in mails:
		print(mail)		
		# sent_mail_to = ses.send_custom_verification_email(
		#    EmailAddress=mail,
		#    TemplateName=language,
		#     )


	