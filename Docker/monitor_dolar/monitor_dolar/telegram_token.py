import telebot
import time
from bcv_values import bcv_usd_consult
from bitven_values import bitven_dolar_consult
from main import main_consult_currency
#from main import dolar_list_values
import requests

file = 'telegram.token' #path to file to read token 

def read_values(file):
        values_l = list()
        with open (file, "r") as f:
            for line in f:
                values_l.append(line.strip()) 

        return values_l[1]


TELEGRAM_TOKEN = read_values(file)

bot = telebot.TeleBot(TELEGRAM_TOKEN)

knownUsers = []  # todo: save these in a file,
userStep = {}  # so they won't reset every time the bot restarts

commands = {  # command description used in the "help" command
    'start'       : 'Get used to the bot',
    'help'        : 'Gives you information about the available commands',
    'bcv'        : 'Get to know official $ worth BCV',
    'dolar'        : 'Get to know official and unofficial $ worth from www.bitven.com',
    'getImage'    : 'A test using multi-stage messages, custom keyboard, and media sending'
}


def get_user_step(uid):
    if uid in userStep:
        return userStep[uid]
    else:
        knownUsers.append(uid)
        userStep[uid] = 0
        print("New user detected, who hasn't used \"/start\" yet")
        return 0



def listener(messages):
    """
    When new messages arrive TeleBot will call this function.
    """
    for m in messages:
        if m.content_type == 'text':
            # print the sent message to the console
            print(str(m.chat.first_name) + " [" + str(m.chat.id) + "]: " + m.text)


bot.set_update_listener(listener)  # register listener


@bot.message_handler(commands=['start'])
def command_start(m):
    cid = m.chat.id
    if cid not in knownUsers:  # if user hasn't used the "/start" command yet:
        knownUsers.append(cid)  # save user id, so you could brodcast messages to all users of this bot later
        userStep[cid] = 0  # save user id and his current "command level", so he can use the "/getImage" command
        bot.send_message(cid, "Hello, stranger, let me scan you...")
        bot.send_message(cid, "Scanning complete, I know you now")
        command_help(m)  # show the new user the help page
    else:
        bot.send_message(cid, "I already know you, no need for me to scan you again!")


######## CONSULTA DOLAR BCV

@bot.message_handler(commands=['bcv'])
def command_bcv(m):
	cid = m.chat.id
	#salida = 'salida por aca'
	bcv_usd_result = bcv_usd_consult()
    # loop for print key as k and value as v for every row in the dictionary 
	for k, v in bcv_usd_result.items():
	    bot.send_message(cid, str(k) +' : ' + str(v))
	

@bot.message_handler(commands=['dolar'])
def command_dolar(m):
    cid = m.chat.id
    main_consult_currency_result = main_consult_currency()
    for responde in main_consult_currency_result:
        bot.send_message(cid, responde)
    	


@bot.message_handler(commands=['help'])
def command_help(m):
    cid = m.chat.id
    help_text = "The following commands are available: \n"
    for key in commands:  # generate help text out of the commands dictionary defined at the top
        help_text += "/" + key + ": "
        help_text += commands[key] + "\n"
    bot.send_message(cid, help_text)  # send the generated help page



try:
    bot.polling(none_stop=True, timeout=123)
except requests.exceptions.ReadTimeout:
    time.sleep(25)
    bot.polling()
except requests.exceptions.ConnectionError:
    time.sleep(25)
    bot.polling()
