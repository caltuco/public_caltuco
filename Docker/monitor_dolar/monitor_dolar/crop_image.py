from PIL import Image

# Opens a image in RGB mode 

def crop_image(path, left, top, right, bottom, name_file):


	img = Image.open(path)
	  
	# Size of the image in pixels (size of original image) 
	# (This is not mandatory) 
	width, height = img.size

	#print(width,height) 
	  
	# Setting the points for cropped image 
	# left = 1
	# top = 0
	# right = 600
	# bottom = 91
	  
	# Cropped image of above dimension 
	# (It will not change orginal image) 
	im1 = img.crop((left, top, right, bottom)) 
	  
	# Shows the image in image viewer 
	im1.save(name_file) 
	#im1.show() 

	return True